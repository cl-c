(defpackage "TEST-C-SYSTEM"
  (:use "CL" "ASDF"))

(in-package "TEST-C-SYSTEM")

(defsystem "TEST-C"
  :components ((:file "test-c"))
  :depends-on ("C"))
