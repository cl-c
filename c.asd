(defpackage "C-SYSTEM"
  (:use "CL" "ASDF"))

(in-package "C-SYSTEM")

(defsystem "C"
  :serial t
  :components ((:file "defpackage")
               (:file "condition")
               (:file "trigraph")
               (:file "lex"))
  :depends-on (:cl-ppcre))
