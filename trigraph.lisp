(in-package "C")

(defvar *replace-trigraphs* nil
  "If not NIL, replace trigraph sequences as described by C99 5.2.1.1 \"Trigraph sequences\".")

(defparameter *trigraph-mapping*
  '((#\! . #\|)
    (#\' . #\^)
    (#\( . #\[)
    (#\) . #\])
    (#\- . #\~)
    (#\/ . #\\)
    (#\< . #\{)
    (#\= . #\#)
    (#\> . #\})))
