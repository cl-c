(defpackage "C"
  (:use "CL")
  (:export "LEX"
           "*REPLACE-TRIGRAPHS*"

           "LEX-CONDITION"
           "C++-COMMENT-LEX-CONDITION"
           "TRIGRAPH-LEX-CONDITION"

           "LEX-ERROR"
           "UNTERMINATED-COMMENT-LEX-ERROR"))
