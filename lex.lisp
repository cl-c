(in-package "C")

(defmacro while (test &body body)
  `(do ()
       ((not ,test))
     ,@body))

(defconstant U+0007 (code-char #x0007))
(defconstant U+0008 (code-char #x0008))
(defconstant U+0009 (code-char #x0009))
(defconstant U+000A (code-char #x000A))
(defconstant U+000B (code-char #x000B))
(defconstant U+000C (code-char #x000C))
(defconstant U+000D (code-char #x000D))

(defun lex (string &key (start 0) end)
  (unless end
    (setf end (length string)))
  (assert (<= 0 start end (length string)))

  (multiple-value-bind (%string pos-vector trigraph-p-vector)
      (translation-phase-1-2 string start end)
    (translation-phase-3 string %string pos-vector trigraph-p-vector)))

(defun lex-full (string &key (start 0) end)
  (translation-phase-5-6-7 (lex string :start start :end end)))

(defun translation-phase-1-2 (string start end)
  (do ((c-list ())
       (pos-list ())
       (trigraph-p-list ())
       (pos start))
      ((>= pos end)
       (values (coerce (nreverse c-list) 'string)
               (coerce (nreverse pos-list) 'simple-vector)
               (coerce (nreverse trigraph-p-list) 'simple-vector)))
    (cond ((char= (char string pos) U+000A)
           (push #\Newline c-list)
           (push pos pos-list)
           (push nil trigraph-p-list)
           (incf pos))

          ((char= (char string pos) U+000D)
           (push #\Newline c-list)
           (push pos pos-list)
           (push nil trigraph-p-list)
           (incf pos (if (and (< (1+ pos) end)
                              (char= (char string (1+ pos)) U+000A))
                         2
                         1)))

          ((and (char= (char string pos) #\\)
                (< (1+ pos) end)
                (or (char= (char string (1+ pos)) U+000A)
                    (char= (char string (1+ pos)) U+000D)))
           (incf pos (if (and (char= (char string (1+ pos)) U+000D)
                              (< (+ pos 2) end)
                              (char= (char string (+ pos 2)) U+000A))
                         3
                         2)))

          ((and (char= (char string pos) #\?)
                (< (+ pos 2) end)
                (char= (char string (1+ pos)) #\?)
                (assoc (char string (+ pos 2)) *trigraph-mapping* :test #'char=))
           (if *replace-trigraphs*
               (let ((tri-char (cdr (assoc (char string (+ pos 2)) *trigraph-mapping* :test #'char=))))
                 (if (and (char= tri-char #\\)
                          (< (+ pos 3) end)
                          (or (char= (char string (+ pos 3)) U+000A)
                              (char= (char string (+ pos 3)) U+000D)))
                     (progn
                       (signal 'trigraph-lex-condition :string string :pos pos)
                       (incf pos (if (and (char= (char string (+ pos 3)) U+000D)
                                          (< (+ pos 4) end)
                                          (char= (char string (+ pos 4)) U+000A))
                                     5
                                     4)))
                     (progn
                       (push tri-char c-list)
                       (push pos pos-list)
                       (push t trigraph-p-list)
                       (incf pos 3))))
               (progn
                 (push #\? c-list)
                 (push pos pos-list)
                 (push t trigraph-p-list)
                 (incf pos))))

          (t
           (push (char string pos) c-list)
           (push pos pos-list)
           (push nil trigraph-p-list)
           (incf pos)))))

(defun translation-phase-3 (string %string pos-vector trigraph-p-vector)
  (do ((pp-token-list ())
       (pos 0)
       (end (length %string))
       (pp-directive-allowed-p t))
      ((>= pos end)
       (nreverse pp-token-list))
    (multiple-value-bind (pp-token inc)
        (get-pp-token %string pos end pp-directive-allowed-p string pos-vector)
      (push (cons pp-token (svref pos-vector pos)) pp-token-list)
      (cond ((or (eql pp-token #\Newline)
                 (and (consp pp-token) (eq (car pp-token) '|#include<|)))
             (setf pp-directive-allowed-p t))
            ((eql pp-token #\Space))
            (t (setf pp-directive-allowed-p nil)))
      (unless (eql pp-token #\Space)
        (loop for i from pos to (1- (+ pos inc))
              when (svref trigraph-p-vector i)
              do (signal 'trigraph-lex-condition :string string :pos (svref pos-vector i))))
      (incf pos inc))))

(defun get-pp-token (string pos end pp-directive-allowed-p %string %pos-vector)
  (let ((c (char string pos))
        (c1 (if (< (1+ pos) end)
                (char string (1+ pos))
                nil))
        (c2 (if (< (+ pos 2) end)
                (char string (+ pos 2))
                nil)))
    (cond ((char= c U+0009) (values #\Space 1))
          ((char= c U+000B) (values #\Space 1))
          ((char= c U+000C) (values #\Space 1))
          ((char= c #\Newline) (values #\Newline 1))
          ((char= c #\Space) (values #\Space 1))
          ((char= c #\!) (cond ((eql c1 #\=) (values '!= 2))
                               (t (values '! 1))))
          ((char= c #\") (%get-string string pos))
          ((char= c #\#) (cond ((eql c1 #\#) (values '|##| 2))
                               (t (if pp-directive-allowed-p
                                      (%get-pp-directive string pos end %string %pos-vector)
                                      (values '|#| 1)))))
          ((char= c #\%) (cond ((eql c1 #\=) (values '%= 2))
                               (t (values '% 1))))
          ((char= c #\&) (cond ((eql c1 #\&) (values '&& 2))
                               ((eql c1 #\=) (values '&= 2))
                               (t (values '& 1))))
          ((char= c #\') (%get-char-const string pos))
          ((char= c #\() (values '|(| 1))
          ((char= c #\)) (values '|)| 1))
          ((char= c #\*) (cond ((eql c1 #\=) (values '*= 2))
                               (t (values '* 1))))
          ((char= c #\+) (cond ((eql c1 #\+) (values '++ 2))
                               ((eql c1 #\=) (values '+= 2))
                               (t (values '+ 1))))
          ((char= c #\,) (values '|,| 1))
          ((char= c #\-) (cond ((eql c1 #\-) (values '-- 2))
                               ((eql c1 #\=) (values '-= 2))
                               ((eql c1 #\>) (values '-> 2))
                               (t (values '- 1))))
          ((char= c #\.) (cond ((and (eql c1 #\.) (eql c2 #\.)) (values '|...| 3))
                               ((find c1 "0123456789") (%get-pp-number string pos))
                               (t (values '|.| 1))))
          ((char= c #\/)
           (cond ((eql c1 #\*) (%get-c-comment string pos %string %pos-vector))
                 ((eql c1 #\/) (%get-c++-comment string pos %string %pos-vector))
                 ((eql c1 #\=) (values '/= 2))
                 (t (values '/ 1))))
          ((find c "0123456789") (%get-pp-number string pos))
          ((char= c #\:) (values '|:| 1))
          ((char= c #\;) (values '|;| 1))
          ((char= c #\<) (cond ((and (eql c1 #\<) (eql c2 #\=)) (values '<<= 3))
                               ((eql c1 #\<) (values '<< 2))
                               ((eql c1 #\=) (values '<= 2))
                               (t (values '< 1))))
          ((char= c #\=) (cond ((eql c1 #\=) (values '== 2))
                               (t (values '= 1))))
          ((char= c #\>) (cond ((and (eql c1 #\>) (eql c2 #\=)) (values '>>= 3))
                               ((eql c1 #\>) (values '>> 2))
                               ((eql c1 #\=) (values '>= 2))
                               (t (values '> 1))))
          ((char= c #\?) (values '? 1))
          ((find c "abcdefghijklmnopqrstuvwxyz_ABCDEFGHIJKLMNOPQRSTUVWXYZ")
           (%get-pp-identifier string pos))
          ((char= c #\[) (values '[ 1))
          ((char= c #\]) (values '] 1))
          ((char= c #\^) (cond ((eql c1 #\=) (values '^= 2))
                               (t (values '^ 1))))
          ((char= c #\{) (values '{ 1))
          ((char= c #\|) (cond ((eql c1 #\=) (values '|\|=| 2))
                               ((eql c1 #\|) (values '|\|\|| 2))
                               (t (values '|\|| 1))))
          ((char= c #\}) (values '} 1))
          ((char= c #\~) (values '~ 1))
          (t (values c 1)))))

(defparameter *include-<-scanner*
  (ppcre:create-scanner "\\A#( |\\t|/\\*.*?\\*/)*include( |\\t|/\\*.*?\\*/)*<([^>\\n]+)>( |\\t|/\\*.*?\\*/)*\\n?" :single-line-mode t))

(defparameter *include-\"-scanner*
  (ppcre:create-scanner "\\A#( |\\t|/\\*.*?\\*/)*include( |\\t|/\\*.*?\\*/)*\"([^\"\\n]+)\"( |\\t|/\\*.*?\\*/)*\\n?" :single-line-mode t))

(defun %get-pp-directive (string start end %string %pos-vector)
  (multiple-value-bind (x-start x-end x-reg-start x-reg-end)
      (ppcre:scan *include-<-scanner* string :start start :end end)
    (when x-start
      (return-from %get-pp-directive (values (cons '|#include<| (subseq string (aref x-reg-start 2) (aref x-reg-end 2))) (- x-end x-start)))))

  (multiple-value-bind (x-start x-end x-reg-start x-reg-end)
      (ppcre:scan *include-\"-scanner* string :start start :end end)
    (when x-start
      (return-from %get-pp-directive (values (cons '|#include"| (subseq string (aref x-reg-start 2) (aref x-reg-end 2))) (- x-end x-start)))))

  (let ((pp-token-list ())
        (pos (1+ start)))
    (do ()
        ((or (eql (caar pp-token-list) #\Newline)
             (>= pos end)))
      (multiple-value-bind (pp-token inc)
          (get-pp-token string pos end nil %string %pos-vector)
        (push (cons pp-token (svref %pos-vector pos)) pp-token-list)
        (incf pos inc)))
    (setf pp-token-list (nreverse pp-token-list))
    (print "")
    (print pp-token-list)
    (print "")
    (values '|#| 1)))

(defun %get-c-comment (string start %string %pos-vector)
  (let ((comment-end (search "*/" string :start2 (+ start 2))))
    (if comment-end
        (values #\Space (- (+ comment-end 2) start))
        (error 'unterminated-comment-lex-error :string %string :pos (svref %pos-vector start)))))

(defun %get-c++-comment (string start %string %pos-vector)
  (signal 'c++-comment-lex-condition :string %string :pos (svref %pos-vector start))
  (values #\Space (- (or (position #\Newline string :start (+ start 2))
                         (length string))
                     start)))

(defparameter *pp-identifier-scanner*
  (cl-ppcre:create-scanner "\\A[a-zA-Z_][a-zA-Z0-9_]*"))

(defun %get-pp-identifier (string start)
  (multiple-value-bind (pp-token-start pp-token-end)
      (cl-ppcre:scan *pp-identifier-scanner* string :start start)
    (assert (= pp-token-start start))
    (assert (< pp-token-start pp-token-end))
    (values (cons 'pp-identifier (subseq string start pp-token-end)) (- pp-token-end start))))

(defparameter *pp-token-scanner*
  ;; "\\A\\.?[0-9]([eEpP][+-]|[0-9a-zA-Z.])*" without :register
  (cl-ppcre:create-scanner
   '(:sequence
     :modeless-start-anchor
     (:greedy-repetition 0 1 #\.)
     (:char-class (:range #\0 #\9))
     (:greedy-repetition 0 nil
      (:alternation
       (:sequence
        (:char-class #\e #\E #\p #\P)
        (:char-class #\+ #\-))
       (:char-class (:range #\0 #\9) (:range #\a #\z) (:range #\A #\Z) #\.))))))

(defun %get-pp-number (string start)
  (multiple-value-bind (pp-token-start pp-token-end)
      (cl-ppcre:scan *pp-token-scanner* string :start start)
    (assert (= pp-token-start start))
    (assert (< pp-token-start pp-token-end))
    (values (cons 'pp-number (subseq string start pp-token-end)) (- pp-token-end start))))

(defparameter *char-const-scanner*
  (cl-ppcre:create-scanner
   '(:sequence
     :modeless-start-anchor
     #\'
     (:alternation
      (:inverted-char-class #\' #\\ #\Newline)
      "\\'" "\\\"" "\\?" "\\\\" "\\a" "\\b" "\\f" "\\n" "\\r" "\\t" "\\v"
      (:sequence #\\ (:greedy-repetition 1 3 (:char-class (:range #\0 #\7))))
      (:sequence "\\x" (:greedy-repetition 1 nil (:char-class (:range #\0 #\9) (:range #\a #\f) (:range #\A #\F)))))
     #\')))

(defun %get-char-const (string start)
  (multiple-value-bind (pp-token-start pp-token-end)
      (cl-ppcre:scan *char-const-scanner* string :start start)
    (if pp-token-start
        (progn
          (assert (= pp-token-start start))
          (assert (< pp-token-start pp-token-end))
          (values (cons 'char-const (subseq string (1+ start) (1- pp-token-end))) (- pp-token-end start)))
        (values #\' 1))))

(defparameter *string-scanner*
  (cl-ppcre:create-scanner
   '(:sequence
     :modeless-start-anchor
     #\"
     (:greedy-repetition 0 nil
      (:alternation
       (:inverted-char-class #\" #\\ #\Newline)
       "\\'" "\\\"" "\\?" "\\\\" "\\a" "\\b" "\\f" "\\n" "\\r" "\\t" "\\v"
       (:sequence #\\ (:greedy-repetition 1 3 (:char-class (:range #\0 #\7))))
       (:sequence "\\x" (:greedy-repetition 1 nil (:char-class (:range #\0 #\9) (:range #\a #\f) (:range #\A #\F))))))
     #\")))

(defun %get-string (string start)
  (multiple-value-bind (pp-token-start pp-token-end)
      (cl-ppcre:scan *string-scanner* string :start start)
    (if pp-token-start
        (progn
          (assert (= pp-token-start start))
          (assert (< pp-token-start pp-token-end))
          (values (cons 'string (subseq string (1+ start) (1- pp-token-end))) (- pp-token-end start)))
        (values #\" 1))))

(defparameter *keyword-hash-table*
  (let ((hash-table (make-hash-table :test #'equal)))
    (dolist (string-keyword '(("auto" . auto)
                              ("break" . break)
                              ("case" . case)
                              ("char" . char)
                              ("const" . const)
                              ("continue" . continue)
                              ("default" . default)
                              ("do" . do)
                              ("double" . double)
                              ("else" . else)
                              ("enum" . enum)
                              ("extern" . extern)
                              ("float" . float)
                              ("for" . for)
                              ("goto" . goto)
                              ("if" . if)
                              ("inline" . inline)
                              ("int" . int)
                              ("long" . long)
                              ("register" . register)
                              ("restrict" . restrict)
                              ("return" . return)
                              ("short" . short)
                              ("signed" . signed)
                              ("sizeof" . sizeof)
                              ("static" . static)
                              ("struct" . struct)
                              ("switch" . switch)
                              ("typedef" . typedef)
                              ("union" . union)
                              ("unsigned" . unsigned)
                              ("void" . void)
                              ("volatile" . volatile)
                              ("while" . while)
                              ("_Bool" . |_Bool|)
                              ("_Complex" . |_Complex|)
                              ("_Imaginary" . |_Imaginary|)))
      (setf (gethash (car string-keyword) hash-table) (cdr string-keyword)))
    hash-table))

(defun translation-phase-5-6-7 (pp-token-list)
  (let ((token-list ())
        (current-string nil)
        (current-string-pos nil))
    (dolist (pp-token-pos pp-token-list)
      (destructuring-bind (pp-token . pos)
          pp-token-pos
        (unless (or (and (consp pp-token) (eq (car pp-token) 'string))
                    (eql pp-token #\Space)
                    (eql pp-token #\Newline))
          (when current-string
            (push (cons (cons 'string current-string) current-string-pos) token-list)
            (setf current-string nil)
            (setf current-string-pos nil)))

        (cond ((eql pp-token #\Space))
              ((eql pp-token #\Newline))

              ((and (consp pp-token)
                    (eq (car pp-token) 'pp-identifier))
               (let ((keyword (gethash (cdr pp-token) *keyword-hash-table*)))
                 (if keyword
                     (push (cons keyword pos) token-list)
                     (push pp-token-pos token-list))))

              ((and (consp pp-token)
                    (eq (car pp-token) 'char-const))
               (push (cons (cons 'char-const (unescape-string (cdr pp-token))) pos) token-list))

              ((and (consp pp-token)
                    (eq (car pp-token) 'string))
               (if current-string
                   (setf current-string (concatenate 'string current-string (unescape-string (cdr pp-token))))
                   (progn
                     (setf current-string (unescape-string (cdr pp-token)))
                     (setf current-string-pos pos))))

              (t (push pp-token-pos token-list)))))
    (when current-string
      (push (cons (cons 'string current-string) current-string-pos) token-list))
    (nreverse token-list)))

(defparameter *unescape-mapping*
  '((#\' . #\')
    (#\" . #\")
    (#\? . #\?)
    (#\\ . #\\)
    (#\a . #.U+0007)
    (#\b . #.U+0008)
    (#\f . #.U+000C)
    (#\n . #.U+000A)
    (#\r . #.U+000D)
    (#\t . #.U+0009)
    (#\v . #.U+000B)))

(defun unescape-string (string)
  (do ((c-list ())
       (pos 0)
       (end (length string)))
      ((>= pos end)
       (coerce (nreverse c-list) 'string))
    (if (char= (char string pos) #\\)
        ;; escape sequence
        ;; #\\ can't be last character
        (let ((c1 (char string (1+ pos))))
          (cond ((assoc c1 *unescape-mapping*)
                 (push (cdr (assoc c1 *unescape-mapping*)) c-list)
                 (incf pos 2))

                ((find c1 "01234567")
                 (multiple-value-bind (val val-end)
                     (parse-integer string :start (1+ pos) :end (min (+ pos 4) end) :radix 8 :junk-allowed t)
                   (push (code-char val) c-list)
                   (setf pos val-end)))

                ((char= c1 #\x)
                 (multiple-value-bind (val val-end)
                     (parse-integer string :start (+ pos 2) :radix 16 :junk-allowed t)
                   (push (code-char val) c-list)
                   (setf pos val-end)))

                (t (error "string ~S, pos ~D" string pos))))
        (progn
          (push (char string pos) c-list)
          (incf pos)))))
