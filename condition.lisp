(in-package "C")

(define-condition lex-condition ()
  ((str :reader lex-condition-string :initarg :string :initform (error "string required") :type string)
   (pos :reader lex-condtiion-pos :initarg :pos :initform (error "pos required") :type fixnum)))

(define-condition trigraph-lex-condition (lex-condition)
  ())

(define-condition c++-comment-lex-condition (lex-condition)
  ())

(define-condition lex-error (error)
  ((str :reader lex-error-string :initarg :string :initform (error "string required") :type string)
   (pos :reader lex-error-pos :initarg :pos :initform (error "pos required") :type fixnum)))

(define-condition unterminated-comment-lex-error (lex-error)
  ())
